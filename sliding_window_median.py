from __future__ import annotations
import math
class Solution:
    def medianSlidingWindow(self, nums: List[int], k: int) -> List[float]:
        self.nums = nums
        self.k = k
        self.window = [None] * self.k
        rtn = []

        for i in range(0,len(nums)-k+1):
            self.copyToWindow(i)
            self.sortWindow()
            rtn.append(self.median(i))
        
        return rtn

    def copyToWindow(self,i):
        for x in range(self.k):
            self.window[x] = self.nums[i+x]

    def sortWindow(self):
        self.window.sort()

    def median(self,i):
        if self.k % 2 ==0:
            halfK = int(k/2)
            return (self.window[halfK] + self.window[halfK - 1])/2
        else:
            return self.window[math.floor(self.k/2)]
    
    def __str__(self):
        return str(self.nums)

        

nums = [1,3,-1,-3,5,3,6,7]
k = 4

s = Solution()
a = s.medianSlidingWindow(nums,k)
print(a)