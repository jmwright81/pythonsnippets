#!/bin/python3

import math
import os
import random
import re
import sys



# Complete the candies function below.
# Complete the maxSubsetSum function below.
def maxSubsetSum(arr):
    incl = 0
    excl = 0
     
    for i in arr: 
          
        # Current max excluding i (No ternary in  
        # Python) 
        new_excl = excl if excl>incl else incl 
         
        # Current max including i 
        incl = excl + i 
        excl = new_excl 
      
    # return max of incl and excl 
    return (excl if excl>incl else incl) 

def dotproduct(arr1,arr2):
    sum = 0
    for i in range(len(arr1)):
        sum += arr1[i]*arr2[i]
    return sum

a = maxSubsetSum([3, 7, 4, 6, 5])

print(dotproduct( (1,0,1,0,0),[3, 7, 4, 6, 5]))

print(a)