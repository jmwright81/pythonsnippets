from __future__ import annotations
import random

class Solution:
    def cherryPickup(self, grid: List[List[int]]) -> int:
       self.grid = grid
       self.size = len(self.grid)
       self.possibleMovesDown = [(1,0),(0,1)]
       self.possibleMovesUp = [(-1,0),(0,-1)]
       self.position = 0,0
       self.cherryCount = 0
       self.pickupCherry()
       self.goingDown = True

       while not self.position == (self.size-1,self.size-1):
           if self.canMove(self.possibleMovesDown):
               self.move(self.possibleMovesDown)
           else:
               return 0
       self.goingDown = False
       while not self.position == (0,0):
           if self.canMove(self.possibleMovesUp):
               self.move(self.possibleMovesUp)
           else:
               return 0


       return self.cherryCount

    def pickupCherry(self):
        print ("at {}".format(self.position))
        if self.grid[self.position[1]][self.position[0]] == 1:
            print ("cherry")
            self.grid[self.position[1]][self.position[0]] = 0
            self.cherryCount += 1
           

    def isCherry(self,move):
        newPosition = self.position[0] + move[0],self.position[1] + move[1]
        return self.grid[newPosition[1]][newPosition[0]]

    def moreCherriesInRow(self):



        if self.goingDown:
            for x in range(self.position[0],self.size):
                if self.grid[self.position[1]][x] == -1:
                    return False
                if self.grid[self.position[1]][x] == 1:
                    return True
        else:
            for x in range(self.position[0],-1,-1):
                if self.grid[self.position[1]][x] == -1:
                    return False
                if self.grid[self.position[1]][x] == 1:
                    return True
            
        return False

    def move(self,moves):

        validMoves = list(filter(lambda move: self.canTakeMove(move), moves))
        cherries = list(map(lambda move: self.isCherry(move), validMoves))
        move = validMoves[cherries.index(max(cherries))]    
 
        if self.moreCherriesInRow():
            move = validMoves[0]

        self.position = (self.position[0] + move[0],self.position[1] + move[1])
        self.pickupCherry()
       

    def canTakeMove(self,move):
        newPosition = self.position[0] + move[0],self.position[1] + move[1]
        return newPosition[0] >= 0 and newPosition[0] < self.size and newPosition[1] >= 0 and newPosition[1] < self.size and self.grid[newPosition[1]][newPosition[0]] != -1

    def canMove(self,moves):
        for move in moves:
            if self.canTakeMove(move):
                return True 
        return False


grid = [[0, 1, -1], [1, 0, -1], [1, 1,  1]]
grid = [[1,1,-1],[1,-1,1],[-1,1,1]]
grid = [[1,1,1,1,0,0,0],[0,0,0,1,0,0,0],[0,0,0,1,0,0,1],[1,0,0,1,0,0,0],[0,0,0,1,0,0,0],[0,0,0,1,0,0,0],[0,0,0,1,1,1,1]]
grid = [[1,1,1,1,1],[1,1,1,1,1],[1,1,-1,1,1],[0,-1,-1,1,1],[1,1,1,1,1]]
s = Solution()
a = s.cherryPickup(grid)
print (a)
print (s.grid)

print ((1,1) + (2,2))
print ([sum(x) for x in zip((1,1) , (2,2))])