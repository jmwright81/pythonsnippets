"""
"Write a program that prints the numbers from 1 to 100. 
But for multiples of three print “Fizz” instead of the number 
and for the multiples of five print “Buzz”. 
For numbers which are multiples of both three and five print “FizzBuzz”."
"""


fizz = 'Fizz'
buzz = 'Buzz'
fizzbuzz = fizz + buzz



for x in range(1,101):
    multiple_of_three = (x % 3 == 0)
    multiple_of_five = (x % 5 == 0)

    if multiple_of_three and multiple_of_five:
        print (fizzbuzz)
    elif multiple_of_three:
        print (fizz)
    elif multiple_of_five:
        print (buzz)
    else:
        print (x)
    
    s = ""
    if x % 3 == 0:
        s += "Fizz"
    if x % 5 == 0:
        s += "Buzz"
    if s == "":
        s = x
    print (s)
    print()

