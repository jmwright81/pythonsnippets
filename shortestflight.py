class Node:
    def __init__(self,id):
        self.id = id
        self.edges = []
        self.cost = 0
        self.depth = 0

class Edge:
    def __init__(self,src,dst,cost):
        self.src = src
        self.dst = dst
        self.cost = cost
import copy

class Solution:
 
    def __init__(self):
        self.edges = []
        self.nodes = {}
 
    def findCheapestPrice(self, n: 'int', flights: 'List[List[int]]', src: 'int', dst: 'int', K: 'int') -> 'int':
 

        for flight in flights:
            for i in range(2):
                if not flight[i] in self.nodes:
                    self.nodes[flight[i]] = Node(flight[i])
            edge = Edge(self.nodes[flight[0]],self.nodes[flight[1]],flight[2])
            self.edges.append(edge)
            self.nodes[flight[0]].edges.append(edge)


        return self.dfs(self.nodes[src],self.nodes[dst], K)


    def dfs(self, srcNode, dstNode, K):
        frontier = []
        visited = set()
        stops = -1

        frontier.append(srcNode)
        
        while (len(frontier)!= 0):
            n = frontier.pop()
            if n.id == dstNode.id:
                return n.cost
            if hops == K:


            if not n.id in visited:
                visited.add(n.id)
                cost = float('inf')
                shortestIndex = None
                for i, edge in enumerate(n.edges):
                    newCost = n.cost + edge.cost
                    if newCost < cost:
                        shortestIndex = i
                        cost = newCost

                if shortestIndex != None:
                    edge = n.edges[shortestIndex]
                    newNode = copy.copy(edge.dst)
                    newNode.cost = n.cost + edge.cost
                    newNode.depth += 1
                    frontier.append(newNode)


        


n = 3
edges = [[0,1,100],[1,2,100],[0,2,500]]
src = 0
dst = 2
k = 1
sol = Solution().findCheapestPrice(n, edges,src,dst,k)
print(sol)