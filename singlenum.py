from __future__ import annotations

class Solution:
    def singleNumber(self, nums: List[int]) -> List[int]:
        
        nums.sort()
        result = []

        for i in range(0,len(nums) - 1,2):
            if nums[i] != nums[i+1]:
                print(i)
                if i % 2 == 0:
                    result.append(nums[i])
                else: 
                    result.append(nums[i])
                i -= 1

        if len(result) == 1:
            print ("last")
            result.append(nums[-1])
        
        return result

    def singleNumber1(self, nums: List[int]) -> List[int]:
        
        result = set()

        for n in nums:
            if n in result:
                result.remove(n)
            else:
                result.add(n)
        
        
        return result

nums = [1,2,1,3,2,5]
nums = [1,2,1,3,2,5]
nums = [0,1,1,2]

solution = Solution()
result = solution.singleNumber(nums)

print (result)