class Solution:
    def shortestSuperstring(self, A: 'List[str]') -> 'str':
       #keep = [s1 for s1 in A for s2 in A if s1 != s2]
       keep = []
       for s1 in A:
            contained = False
            for s2 in A:
                if s1 == s2:
                   continue
                if s1 in s2:
                    contained = True
            
            if not contained:
                keep.append(s1)
                
       return "".join(keep)

a = ["alex","loves","leetcode"]
a = ["catg","ctaagt","gcta","ttca","atgcatc"]

b = Solution().shortestSuperstring(a)
print(b)

#a = [x for x in range(10) for y in range(10) if x % 2 != 0]
#print (a)