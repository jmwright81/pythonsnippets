gas  = [1,2,3,4,5]
cost = [3,4,5,1,2]

class Solution:

   

    def canCompleteCircuit(self, gas: 'List[int]', cost: 'List[int]') -> 'int':
        size = len(gas)

        for startPos in range(size):
            print("\n")
            tank = 0
            complete = True    
            for station in range (startPos,startPos+size):
                #print (startPos,station,gas[station % size])
                tank += gas[station % size]
                tank -= cost[station % size]
                if tank < 0:
                    complete = False
                    break
            if complete:
                return startPos       
        return -1    

a = Solution().canCompleteCircuit(gas,cost)
print(a)

z = [a+b for a in range(2) for b in range(3)]
print(sum(z))