from __future__ import annotations
# Definition for a binary tree node.

class Solution:
    def topKFrequent(self, nums: List[int], k: int) -> List[int]:
 
        size = len(nums)

        if size == 1:
            return nums

        nums.sort()

        count = 1
        rtn = [nums[0]]

        if count == k:
            return rtn

        for x in range(size-1):
            print (nums[x] , nums[x+1])
            if nums[x] != nums[x+1]:
                count += 1
                rtn.append(nums[x+1])
                if count == k: 
                    break

            
        return rtn

k = 2
nums = [1,1,1,2,2,3]
#nums = [-1,-1]
nums = [3,0,1,0]
nums = [4,1,-1,2,-1,2,3]
k = 2
solution = Solution()
res = solution.topKFrequent(nums,k)
print(res)