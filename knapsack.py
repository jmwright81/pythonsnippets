
values = [60,100,120]
weights = [10,20,30]
size = 50


def maxValue(values,weights,size):

    knapsack = [0] * 51

    for v,w in zip(values,weights):

        for i in range(w,size+1):
            print (i, size-i)

            if v > knapsack[i] + knapsack[size-i]:
                knapsack[i] = v


    return knapsack[size]

answer = maxValue(values,weights,size)
print (answer)