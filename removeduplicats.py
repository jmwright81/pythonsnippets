from __future__ import annotations

class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        l = len (nums)
        returnSize = l
        i = 0
        while i + 1 < l and i + 2 < l:
            if nums[i] == nums[i+1] == nums[i+2] and nums[i] != 'x':
                for j in range(i+2,l-1):
                    nums[j] = nums[j+1]
                nums[l-1] = 'x'
                returnSize -= 1
            else:
                i += 1       
        return returnSize


nums = [0,0,1,1,1,1,2,3,3]
a = Solution().removeDuplicates(nums)
print(a)
print(nums)