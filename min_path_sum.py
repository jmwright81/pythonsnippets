from __future__ import annotations
import time
class Node:
    

    def __init__(self, position, score):
        self.children = []
        self.position = position
        self.score = score

    def __str__(self):
        return "Position {} , Score {}, Children {}".format(self.position,self.score, len(self.children))

    def getScore(self):
        return self.score

class Solution:

    # right or down
    POSSIBLE_MOVES = [(1,0),(0,1)]
    STARTING_POS = (0,0)
    ENDING_POS = None

    def minPathSum(self, grid: List[List[int]]) -> int:
        self.grid = grid
        Solution.ENDING_POS = ( len(grid[0]) -1, len(grid) - 1)
        
        if Solution.ENDING_POS == Solution.STARTING_POS:
            return self.grid[Solution.STARTING_POS[1]][Solution.STARTING_POS[0]]

        paths = []
        frontier = []
        solutions = []

        n = Node(Solution.STARTING_POS,grid[Solution.STARTING_POS[1]][Solution.STARTING_POS[0]] )
        paths.append(n)

        while len(paths) > 0:
            frontier.clear()
            for n in paths:
                self.makePossibleMoves(n)
                frontier.extend(n.children)
            print ("\n Frontier size {} ".format(len(frontier)))

            #time.sleep(1)
            for n in frontier:
                if n.position == Solution.ENDING_POS:
                    solutions.append(n)
            paths.clear()
            paths.extend(frontier)

        if len(solutions) > 0:
            return min (solutions, key=lambda x : x.score).score
        else:
            return 0

    def makePossibleMoves(self,n: Node) -> List[Node]:
        for move in Solution.POSSIBLE_MOVES:
            if self.canMove(move, n):
                newPostion = ( n.position[0]+move[0] , n.position[1]+move[1] )
                newScore = n.score + self.grid[newPostion[1]][newPostion[0]]
                newN = Node(newPostion, newScore)
                n.children.append(newN)
        
    def canMove(self, move, n: Node) -> bool:
        newX = n.position[0]+move[0]
        newY = n.position[1]+move[1]
        return newX >= 0 and newX < len(self.grid[0]) and newY >= 0 and newY < len(self.grid) 

grid = [  [1,3,1],  [1,5,1],  [4,2,1]]

s = 12
grid = [[1 for x in range(s)] for y in range(s)] 
#grid = [[1]]

grid1 = [[7,1,3,5,8,9,9,2,1,9,0,8,3,1,6,6,9,5],[9,5,9,4,0,4,8,8,9,5,7,3,6,6,6,9,1,6],[8,2,9,1,3,1,9,7,2,5,3,1,2,4,8,2,8,8],[6,7,9,8,4,8,3,0,4,0,9,6,6,0,0,5,1,4],[7,1,3,1,8,8,3,1,2,1,5,0,2,1,9,1,1,4],[9,5,4,3,5,6,1,3,6,4,9,7,0,8,0,3,9,9],[1,4,2,5,8,7,7,0,0,7,1,2,1,2,7,7,7,4],[3,9,7,9,5,8,9,5,6,9,8,8,0,1,4,2,8,2],[1,5,2,2,2,5,6,3,9,3,1,7,9,6,8,6,8,3],[5,7,8,3,8,8,3,9,9,8,1,9,2,5,4,7,7,7],[2,3,2,4,8,5,1,7,2,9,5,2,4,2,9,2,8,7],[0,1,6,1,1,0,0,6,5,4,3,4,3,7,9,6,1,9]]

print(len(grid1))
print(len(grid1[0]))


a = Solution().minPathSum(grid)
print(a)
#print (Solution.POSSIBLE_MOVES)
#print (Solution.ENDING_POS)