from __future__ import annotations

class Solution:
    def canPartition2(self, nums: List[int]) -> bool:
        nums.sort()
        snu = sum(nums)
        if snu & 1: return False
        target = snu // 2
        dp = [False for i in range(target + 1)]
        dp[0] = True
        for num in nums:
            for j in range(target,-1,-1):
                if j >= num:
                    print ("num={}  j={}  target={}".format(num,j,target))
                    dp[j] = dp[j] or dp[j - num]
                    print ("set dp[{}]={}\n".format(j,dp[j]))
                else:
                    print ("break") 
                    break
        return dp[target]

    def canPartition(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        possible_sums = {0:0}
        new_possible_sums = {}
        for n in nums:
            for v in possible_sums:
                if not v+n in new_possible_sums:
                    new_possible_sums[v+n] = 1
                else:
                    new_possible_sums[v+n] += 1
            for new_sum in new_possible_sums:
                if not new_sum in possible_sums:
                    possible_sums[new_sum] = new_possible_sums[new_sum]
                else:
                    possible_sums[new_sum] += new_possible_sums[new_sum]
            new_possible_sums.clear()
            print (n,possible_sums)
        return (sum(nums) / 2.)  in possible_sums  
    

ints =[1, 5, 11, 5]

sol = Solution()
result = sol.canPartition(ints)
print (result)
d = {'a':1}
d['a'] +=1
print(d)
