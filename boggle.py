import copy
import string
import random
class Grid:

    OCCUPIED = 1
    EMTPY = 0


    def __init__(self,size):
        self.size = size
        self.grid = [[Grid.EMTPY for i in range(self.size)] for j in range(self.size)]
        self.children = []
        self.parent = None
        self.pos = None
        self.moves = [(i,j) for i in range(-1,2) for j in range(-1,2) if not i==j ]

    def __str__(self):
        val = "Size {}, Pos {}\n".format(self.size, self.pos)
        for row in self.grid:
            val += str(row) + '\n'
        val += str(self.getAllowedMoves())
        return val
    
    '''
    Visit a grid location
    i horizontal index
    j vertical index
    '''
    def visit(self,i,j):
        assert(self.grid[j][i] == Grid.EMTPY)
        self.pos = i,j
        self.grid[j][i] = Grid.OCCUPIED


    def getAllowedMoves(self):
        allowed = []
        for move in self.moves:
            i = move[0] + self.pos[0]
            j = move[1] + self.pos[1]
            if i >= 0 and i < self.size and j >= 0 and j < self.size and self.grid[j][i] == Grid.EMTPY:
                allowed.append(move)     
        return allowed
    
    def makeMoves(self):
        for move in self.getAllowedMoves():
            childGrid = copy.deepcopy(self)
            childGrid.parent = self
            childGrid.children = []
            childGrid.visit(move[0] + self.pos[0], move[1] + self.pos[1])
            self.children.append(childGrid)
    
    def isComplete(self):
        return len(self.getAllowedMoves()) == 0

    


g = Grid(3)
g.visit(1,0)
print(g)         
print(g.getAllowedMoves())
frontier = []
frontier.append(g)
solutions = []

while len(frontier) != 0:
    #print("Frontier size {} \n".format(len(frontier)))
    g = frontier.pop()
    #print(g)
    g.makeMoves()
    frontier.extend(g.children)
    if g.isComplete():
        solutions.append(g)

print (len(solutions))

size = 3
letter  = [[random.choice(string.ascii_lowercase) for i in range(size)] for j in range(size)]
print (letter)

for s in solutions:
    word = []
    while (s != None):
        word.append(letter[s.pos[1]][s.pos[0]])
        s = s.parent
    word.reverse()
    word = "".join(word)
    print(word)
