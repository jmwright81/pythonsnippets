

class Thing:

    def __init__(self,value):
        self.value = value

    def __str__(self):
        return str(self.value)
    
    def __eq__(self, other):
        if not self or not other:
            return False
        return self.value == other.value
    
    def __ne__(self,other):
        return not self.__eq__(other)

    def __hash__(self):
        #print ("hash {}".format(hash(self.value)))
        return hash(self.value)


thing1 = Thing(1)
thing2 = Thing(1)
print(thing1)
print(thing2)
print(thing1 == thing2)

s = set()
s.add(thing1)
s.add(thing2)

print (len(s))
print(s)