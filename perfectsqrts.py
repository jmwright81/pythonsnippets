

class Node:
    def __init__(self, x):
        self.val = x
        self.next = []
        self.level = 1
    def __str__(self):
        return "{} -> {}".format(self.val,self.next.val)
import math    

class Solution:

    def numSquares(self, n: 'int') -> 'int':
        if n <= 3:
            return n
        squares = { i * i for i in range(1, math.floor(math.sqrt(n)) + 1) }
        sums = squares
        for i in range(1, n):
            if n in sums:
                return i
            sums = { a + b for a in squares for b in sums if a + b <= n }

  


sol = Solution().numSquares(77)
print(sol)

s = {1,1,2}
print(s)

d = {'a':1, 'b':2, 'c':3}
for key, value in d.items():
    print(f"{key}: {value}")

s = set()
s.add(1)
s.add(1)
s.add(1)
print(s)