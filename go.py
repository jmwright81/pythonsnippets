print("Hello World")
print("a")
import string
print (string.ascii_lowercase)
import numpy as np
a = np.array([1,2])
print (a *11)

class Grid:

    def __init__(self,size):
        self.size = size

    def __str__(self):
        return "I am a grid of size {}".format(self.size)

g = Grid(30)
print(g)

def add(a,b) -> str:
    return a + b

print (add(10,20))

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None
    def __str__(self):
        return "{} -> {}".format(self.val,self.next.val)

l2 = ListNode(2)
l4 = ListNode(4)
l3 = ListNode(3)
l2.next = l4
l4.next = l3
list1 = l2 

l5 = ListNode(5)
l6 = ListNode(6)
l42 = ListNode(4)
l5.next = l6
l6.next = l42
list2 = l5

import math
class Solution:
    def listToNum(self,list: 'ListNode') -> int:
        nums = []
        while True:
            nums.append(str(list.val))
            if list.next == None:
                break
            list = list.next
        nums.reverse()
        return int(''.join(nums))

    def numToList(self, num : int) -> 'ListNode':
        lastNode = None
        firstNode = None
        for n in reversed(str(num)):
            node = ListNode(n)
            if firstNode == None:
                firstNode = node
            if lastNode != None:
                lastNode.next = node
            lastNode = node
        return firstNode
    """
    Invert
    """
    def toNum(self, node: 'ListNode') -> 'ListNode':
        num = 0
        base = 1
        while (True):
            if node == None:
                break;
            num += node.val * base
            if node.next == None:
                break;
            node = node.next
            base *= 10

        return num
            
    def numToListQuick(self, num : int) -> 'ListNode':
        base = 100
        a = math.floor(num/base)

        node1 = ListNode(a)

        num -= a * base
        base /= 10
        b = math.floor(num/base)

        node2 = ListNode(b)
        num -= b * base
        base /= 10
        c = math.floor(num/base)
        node3 = ListNode(c)

        node3.next = node2
        node2.next = node1
        return node3



    def addTwoNumbers(self,l1: 'ListNode', l2: 'ListNode') -> 'ListNode':
        return self.numToListQuick(self.toNum(l1) + self.toNum(l2))
      
a = Solution().addTwoNumbers(list1,list2)
print(a)


