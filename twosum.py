numbers = [2,7,11,15]
target = 9

class Solution:

   def twoSum1(self, numbers: 'List[int]', target: 'int') -> 'List[int]':

        start = 0
        end = len(numbers)-1

        while start < end :
            sum = numbers[start] + numbers[end]

            if sum == target:
                return [start + 1, end+1]

            elif sum < target:
                previous = numbers[start]
                while (previous == numbers[start]):
                    start = start + 1

            elif sum > target:
                previous = numbers[end]
                while (previous == numbers[end]):
                    end = end - 1
    
        return "No solution found!"            


    def twoSum(self, numbers: 'List[int]', target: 'int') -> 'List[int]':
        size = len(numbers)
        i = size - 1
        previousi = None
        while(i >= 0):
            while (numbers[i] == previousi and i >= 0):
                i -= 1
            j = 0
            previousj = None
            while (j < size):
                while (numbers[j] == previousj and j < size):
                    j += 1
                if numbers[j] + numbers[i] == target:
                    if i < j:
                        return [i+1,j+1]
                    if j < i:
                        return [j+1,i+1]
                previousj = numbers[j]
                j += 1
            previousi = numbers[i]
            i -= 1
    
        return "No solution found!"
        
        

sol = Solution().twoSum(numbers,target)
print(sol)

